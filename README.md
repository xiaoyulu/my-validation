# 验证器

摘取 laravel 验证器，将其进行封装，让其它 PHP 框架可以使用 laravel 的验证器。


## 安装

[lilei/my-validator](https://packagist.org/packages/lilei/my-validator)

```shell
$ composer require lilei/my-validator
```


# package

- illuminate/container
- illuminate/validation
- phpunit/phpunit
- overtrue/laravel-lang - 引入了语言包



# 使用

可以直接参考 laravel 的 [文档](https://learnku.com/docs/laravel/8.5/validation/10378)。语言默认使用的是 `zh-CN`。

`tests/MyValidatorTest.php`

```php
$MyValidator = new MyValidator;
//$bool = $MyValidator->setLang('en')->validator($data, $rules, $customAttributes);
$bool = $MyValidator->validator($data, $rules, $messages, $customAttributes);
var_dump($bool);
print_r($MyValidator->getMessage());
```



## 设置路径与语言

```php
$path = "D:/my-validator/src/lang";// 文件夹的绝对路径
$lang = "zh-CN";// 语言目录名
$MyValidator->setPath($path)->setLang($lang);
```

## 验证参数

`$data` 验证数据

```php
$data = ['account' => "zhangsan"];
```

`$rules` 验证规则

```php
$rules = ['account' => "required|max:12"];
```

`$messages` 自定义消息

```php
$messages = [
    'account.required' => ":attribute 是必填",
    'account.max:12'   => ":attribute 长度太长了，要小于 12",
];
```

`$customAttributes` 自定义属性

```php
$customAttributes = ['account' => "神秘账户"];
```

