<?php
declare(strict_types=1);

namespace LiLei\Validation\Tests;

use PHPUnit\Framework\TestCase;
use LiLei\Validation\MyValidator;

class MyValidatorTest extends TestCase
{
    // vendor/bin/phpunit tests/MyValidatorTest.php --filter test
    public function test()
    {
        $data = [
            'account'   => "",
            'password'  => "password",
        ];
        $rules      = [
            'account'   => "required|max:12",
            'password'  => "required|max:255",
        ];
        $messages = [
            'account.required'   => ":attribute 大胆，不能是空的",
        ];
        $customAttributes = [
            'account'   => "神秘账户",
            "password"  => "密码",
        ];

        $MyValidator = new MyValidator;
        //$bool = $MyValidator->setLang('en')->validator($data, $rules, $customAttributes);
        $bool = $MyValidator->validator($data, $rules, $messages, $customAttributes);
        var_dump($bool);
        print_r($MyValidator->getMessage());
        print_r($MyValidator->getMessage(false));
    }// test() end
}